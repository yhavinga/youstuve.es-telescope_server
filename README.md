This is a git repo created to use code from
http://yoestuve.es/blog/communications-between-python-and-stellarium-stellarium-telescope-protocol/#more-313
as a submodule in other projects.

For instructions and license information please refer to the blog post
referenced above.
